# Use-Case Bodenfeuchte

## Projektbeschreibung

In diesem Use-Case wurde die Integration des "Tektelic Kiwi Agriculture Sensor" durchgeführt. Der Sensor misst die Bodenfeuchte und -temperatur.

Der Use-Case wurde auf der FIWARE-basierten [Urban Dataspace Platform](https://gitlab.com/urban-dataspace-platform/core-platform) entwickelt und integriert.

## Installationsanleitung

Um den Anwendungsfall zu installieren sind folgende Schritte nötig:  
1. Eine lokale Maschine für die Installation ist vorzubereiten (Siehe https://gitlab.com/urban-dataspace-platform/core-platform/-/blob/master/00_documents/Admin-guides/cluster.md).
2. Eine NodeRED-Instanz deployen oder eine vorhandene Instanz verwenden:
    - Deployment analog zu den *demo_usecases*: https://gitlab.com/urban-dataspace-platform/use_cases/demo_usecases/deployment
3. *flow.json* in der NodeRED-Instanz deployen.

## Gebrauchsanweisung

Der NodeRED-Flow ruft die Daten per HTTP-Request von der ElementIOT-API ab. Dabei kann eine Folder-ID angegeben werden, um alle Devices aus einem Ordner mit einem Request abzufragen. Für die ElementIOT-API ist ein API-Key erforderlich.

Diese Daten werden in das NGSI-kompatible [GreenspaceRecord Smart-Data-Model](https://github.com/smart-data-models/dataModel.ParksAndGardens/tree/master/GreenspaceRecord) umgewandelt und an die Datenplattform gesendet:

```
{
   "id":"urn:nsgi-ld:GreenspaceRecord:e162387f-3b80-4cd2-9793-430c6475d213",
   "type":"GreenspaceRecord",
   "dateObserved":{
      "type":"DateTime",
      "value":"2024-05-03T12:23:54.558Z"
   },
   "name":{
      "type":"Text",
      "value":"Kiwi_T0072"
   },
   "soilMoisturePressure":{
      "type":"Number",
      "value":0.22
   },
   "soilMoisturePressureRaw":{
      "type":"Number",
      "value":0.187984
   },
   "soilMoistureFrequency":{
      "type":"Number",
      "value":7017
   },
   "soilTemperature":{
      "type":"Number",
      "value":11.74
   },
   "illuminance":{
      "type":"Number",
      "value":115
   }
}
```

## Kontaktinformationen

Dieses Projekt wurde von der [Hypertegrity AG](https://www.hypertegrity.de/) entwickelt.

## Lizenz

Dieses Projekt wird unter der EUPL 1.2 veröffentlicht.

## Link zum Original-Repository

https://gitlab.com/urban-dataspace-platform/use_cases/integration/bodenfeuchte/nr-bodenfeuchte-tektelic-kiwi